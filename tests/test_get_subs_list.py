# Copyright 2017 Zdenek Kraus <zdenek.kraus@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.import reformat

from unittest import TestCase

import reformat


class TestGetSubsListTests(TestCase):
    def test_notfound(self):
        s = 'Just text '
        exp = [
            ('', '', '', 'Just text '),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a(self):
        s = 'Test %{a}'
        exp = [
            ('', '', '', 'Test '),
            ('a', '', '', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_npt(self):
        s = 'Test %{a|}'
        exp = [
            ('', '', '', 'Test '),
            ('a', '', '', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_pt(self):
        s = 'Test %{a|b}'
        exp = [
            ('', '', '', 'Test '),
            ('a', 'b', '', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_pt_nptn(self):
        s = 'Test %{a|b|}'
        exp = [
            ('', '', '', 'Test '),
            ('a', 'b', '', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_npt_ptn(self):
        s = 'Test %{a||c}'
        exp = [
            ('', '', '', 'Test '),
            ('a', '', 'c', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_pt_ptn(self):
        s = 'Test %{a|b|c}'
        exp = [
            ('', '', '', 'Test '),
            ('a', 'b', 'c', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_pts(self):
        s = 'Test %{a|%s}'
        exp = [
            ('', '', '', 'Test '),
            ('a', '%s', '', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_pts_ptns(self):
        s = 'Test %{a|%s|%s}'
        exp = [
            ('', '', '', 'Test '),
            ('a', '%s', '%s', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_pts_nptn(self):
        s = 'Test %{a|%s|}'
        exp = [
            ('', '', '', 'Test '),
            ('a', '%s', '', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_npts_ptns(self):
        s = 'Test %{a||%s}'
        exp = [
            ('', '', '', 'Test '),
            ('a', '', '%s', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)

    def test_a_npt_nptn(self):
        s = 'Test %{a||}'
        exp = [
            ('', '', '', 'Test '),
            ('a', '', '', ''),
        ]
        x = reformat.get_subs_list(s)
        self.assertEqual(exp, x)
