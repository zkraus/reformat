# Copyright 2017 Zdenek Kraus <zdenek.kraus@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
# http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from distutils.core import setup

setup(
    name='reformat',
    version='1.0.3',
    packages=['reformat'],
    url='https://bitbucket.org/zkraus/reformat',
    license='Apache-2.0',
    author='Zdenek Kraus',
    author_email='zdenek.kraus@gmail.com',
    description='String substitution tool using readable notation and safe evaluation',
    download_url='https://bitbucket.org/zkraus/libracmp/get/libracmp-1.0.3.tar.gz'
)
